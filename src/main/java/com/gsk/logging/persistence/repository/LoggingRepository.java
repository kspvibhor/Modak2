/**
 * 
 */
package com.gsk.logging.persistence.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.gsk.logging.persistence.entity.Logging;

/**
 * @author sb422136
 *
 */
@Repository
public interface LoggingRepository extends MongoRepository<Logging, String> {
	List<Logging> findByMudId(String mudId);
	List<String> findDistinctByService();
}
