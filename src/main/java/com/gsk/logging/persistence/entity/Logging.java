/**
 * 
 */
package com.gsk.logging.persistence.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author sb422136
 *
 */
@Document(collection = "log")
public class Logging {
	@Id
	private String id;
	@Field("mud_id")
	private String mudId;
	@Field("action")
	private String action;
	@Field("service")
	private String service;
	@Field("content")
	private String content;
	@Field("instance")
	private String instance;
	@Field("response_status_code")
	private Integer responseStatusCode;
	@Field("response_status")
	private String responseStatus;
	@Field("action_date")
	private String actionDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMudId() {
		return mudId;
	}

	public void setMudId(String mudId) {
		this.mudId = mudId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public String getActionDate() {
		return actionDate;
	}

	public void setActionDate(String actionDate) {
		this.actionDate = actionDate;
	}

	@Override
	public String toString() {
		return "Logging [id=" + id + ", mudId=" + mudId + ", action=" + action + ", service=" + service + ", content="
				+ content + ", instance=" + instance + ", responseStatusCode=" + getResponseStatusCode()
				+ ", responseStatus=" + responseStatus + ", actionDate=" + actionDate + "]";
	}

	public Integer getResponseStatusCode() {
		return responseStatusCode;
	}

	public void setResponseStatusCode(Integer responseStatusCode) {
		this.responseStatusCode = responseStatusCode;
	}

}
