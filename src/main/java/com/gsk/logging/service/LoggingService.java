/**
 * 
 */
package com.gsk.logging.service;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gsk.common.constants.MicroserviceNames;
import com.gsk.common.web.dto.LoggingDTO;
import com.gsk.logging.persistence.entity.Logging;
import com.gsk.logging.persistence.repository.LoggingRepository;

/**
 * @author sb422136
 *
 */
@Component
public class LoggingService {
	private @Autowired LoggingRepository loggingRepository;
	private ModelMapper modelMapper = new ModelMapper();

	public void add(LoggingDTO loggingDTO) {
		Logging logging = modelMapper.map(loggingDTO, Logging.class);
		loggingRepository.save(logging);
	}

	public LoggingDTO load(String id) {
		Logging logging = loggingRepository.findOne(id);
		return modelMapper.map(logging, LoggingDTO.class);
	}

	public List<LoggingDTO> loadByMudId(String mudId) {
		List<LoggingDTO> response = new ArrayList<>();
		for (Logging logging : loggingRepository.findByMudId(mudId)) {
			response.add(modelMapper.map(logging, LoggingDTO.class));
		}
		return response;
	}

	public List<String> getServices() {
		List<String> microServices = new ArrayList<>();
		for (MicroserviceNames microserviceNames : MicroserviceNames.values()) {
			microServices.add(microserviceNames.toString());
		}
		return microServices;
	}
}
