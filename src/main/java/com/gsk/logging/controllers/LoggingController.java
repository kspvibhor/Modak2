/**
 * 
 */
package com.gsk.logging.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gsk.common.web.dto.LoggingDTO;
import com.gsk.logging.service.LoggingService;

/**
 * @author sb422136
 *
 */
@RestController
@RequestMapping("/logging")
public class LoggingController{
	private @Autowired LoggingService loggingService;

	@SuppressWarnings("rawtypes")
	@PostMapping
	public ResponseEntity create(@RequestBody LoggingDTO loggingDTO) {
		try {
			loggingService.add(loggingDTO);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/{uuid}")
	public ResponseEntity<LoggingDTO> load(@PathVariable("uuid") String id) {
		try {
			return new ResponseEntity<LoggingDTO>(loggingService.load(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/mudId/{mudId}")
	public ResponseEntity<List<LoggingDTO>> loadByMudId(@PathVariable("mudId") String mudId) {

		try {
			return new ResponseEntity<List<LoggingDTO>>(loggingService.loadByMudId(mudId), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/services")
	public ResponseEntity<List<String>> loadServiceNames() {
		try {
			return new ResponseEntity<>(loggingService.getServices(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
