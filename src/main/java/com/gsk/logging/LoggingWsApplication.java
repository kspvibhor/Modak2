package com.gsk.logging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * @author sb422136
 *
 */
@SpringBootApplication
@EnableSwagger2
@EnableEurekaClient
public class LoggingWsApplication {
	public static void main(String[] args) {
		SpringApplication.run(LoggingWsApplication.class, args);
	}

	@Bean
	public Docket newsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("logging").apiInfo(apiInfo()).select().build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Logging MicroService").description("Logging MicroService")
				.termsOfServiceUrl("http://gsk.com").contact("Santosh Bhima").version("2.0").build();
	}
}
